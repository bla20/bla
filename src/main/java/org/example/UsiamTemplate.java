package org.example;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class UsiamTemplate {
   
    public static void main(String[] args) {
        try {
            readDocxTables("C:\\Users\\Vaso\\Documents\\programiranje\\SanjaUploader\\create_table.docx");
        } catch (Exception e) {
            System.out.println("jbg");
        }
    }

    public static void readDocxTables(String docxFilePath) throws FileNotFoundException, IOException {
        ArrayList<XWPFTable> listaTabela = null;
        XWPFDocument doc = new XWPFDocument(new FileInputStream(docxFilePath));
        for (XWPFTable table : doc.getTables()) {
            listaTabela.add(table);
            for (XWPFTableRow row : table.getRows()) {
                System.out.println(row.getCell(0).getText());
            }
        }
    }
}
