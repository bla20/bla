package org.example.Tabela;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

//sadrzi Tabele iz Worda
//mozda da se podeli u dve Classe TabeleizWorda i TabeleTemplate ili
// da se readTmplates prebaci Podaci Tabela Templet
//a da imamo trevcu moyda klasu koja hendluje poveyivanje i exception
public class TableIzWorda {
    ArrayList<XWPFTable> tabeleIzWorda = new ArrayList<>();//imena izvestaja iz koga izvlacimo
    ArrayList<PodaciTabelaTepmlate> TabelaPodaciTemplate = new ArrayList<>();

    public void readDocxTables(XWPFDocument doc) throws FileNotFoundException, IOException {

        for (XWPFTable table : doc.getTables()) {
            this.tabeleIzWorda.add(table);

        }
    }

    //napravim Podaci koji imaju koji je ivestaj i koji je column i koji i podatke
    //Privremeno Menjaj!!!!!!!!!!!! Trebalo bi da se delegiramozda i PodaciTabeleTenplate
    public void readTmplates(String docxFilePath) throws FileNotFoundException, IOException {

        XWPFDocument doc = new XWPFDocument(new FileInputStream(docxFilePath));
        for (XWPFTable table : doc.getTables()) {
            PodaciTabelaTepmlate p = new PodaciTabelaTepmlate();
            for (int i = 1; i < table.getRow(3).getTableCells().size(); i++) {
                if (table.getRow(0).getCell(0).getText().equals("Izvestaj")) {
                    p.izvestaji.add(table.getRow(0).getCell(i).getText());

                }
            }
            for (int i = 1; i < table.getRow(3).getTableCells().size(); i++) {
                if (table.getRow(1).getCell(0).getText().equals("Column")) {
                    p.kojiColumn.add(table.getRow(1).getCell(i).getText());
                }
            }

            p.table = table; //ovo je tabela koju smo ucitali u objekat PodaciTabelaTemplate iposke ga dodamo u listu
            TabelaPodaciTemplate.add(p); //ucitanu tabelu dodamo
        }

    }

    public void updateTableIzWordaWithSchema() {

        for (int iteratorWord = 0; iteratorWord < tabeleIzWorda.size(); iteratorWord++) {
            XWPFTable iteratortabelaWord = tabeleIzWorda.get(iteratorWord);
            for (
                    int iteratorTabele = 0; iteratorTabele < TabelaPodaciTemplate.size(); iteratorTabele++
            ) {
                PodaciTabelaTepmlate iteratorPOdac = TabelaPodaciTemplate.get(iteratorTabele);

                //za svaku tabelu iz word file uporedi sa tabelom iz templeta ako su prve kolone iste nastavi
                if (iteratorPOdac.uporedim(iteratortabelaWord)) {
                   /* System.out.println(iteratortabelaWord.getRows().size());
                    System.out.println(iteratortabelaWord.getRow(0).getTableCells().size());*/
                    iteratorPOdac.napunimMatricu();
                    //column
                    for (int i = 0; i < iteratortabelaWord.getRow(0).getTableCells().size(); i++) {

                        //rows
                        for (int j = 0; j < iteratortabelaWord.getRows().size(); j++) {
                            System.out.println(iteratortabelaWord.getRow(j).getCell(i).getText());
                            if (iteratorPOdac.array2DwithString.Array2D.get(j).get(i) == null) {
                                continue;
                            } else if (iteratorPOdac.array2DwithString.Array2D.get(j).get(i).equals("preskoci")) {
                                continue;
                            } else if (iteratorPOdac.array2DwithString.Array2D.get(j).get(i).equals("promeni")) {
                                this.replace(j, i, "promeni", iteratortabelaWord);
                                continue;
                            } else if (iteratorPOdac.array2DwithString.Array2D.get(j).get(i).equals("ukupno")) {
                                this.replace(j, i, "ukupno", iteratortabelaWord);

                                continue;
                            } else {
                                this.replace(j, i, iteratorPOdac.array2DwithString.Array2D.get(j).get(i), iteratortabelaWord);
                                continue;
                            }
                        }
                    }

                    /*System.out.println(iteratorPOdac.array2DwithString.toString());*/

                }
            }
        }
    }


    //ovo je naruznija formula ikada ali nmg da nadjem  negde kako da promenim vrednost celij
    //Privremeno Menjaj!!!!!!!!!!!!1
    //iz nekog razloga nmg nadjem kako da promenim na XWPFPtable celiji promenim celu vrednost
    //non stop samo dodaje
public void replace(int j,int i,String rec,XWPFTable wordTabela){
    List<XWPFParagraph> broj = wordTabela.getRow(j).getCell(i).getParagraphs();
    for (int k = 0; k < wordTabela.getRow(j).getCell(i).getParagraphs().size(); k++) {
        System.out.println("ovoooooooooooooooooooooooooo je br"+k);
        XWPFParagraph p=broj.get(k);
        List<XWPFRun> r =p.getRuns();
        for (int l = 0; l < r.size(); l++) {
            System.out.println("ovoooooooooooooooooooooooooo je br"+l);
            XWPFRun op=r.get(l);
            if(l>0||k>0){
                op.setText("",0);
                continue;
            }

            op.setText(rec,0);
        }
    }
   /* for (XWPFParagraph p : wordTabela.getRow(j).getCell(i).getParagraphs()) {

        System.out.println("ovoooooooooooooooooooooooooo je br"+wordTabela.getRow(j).getCell(i).getParagraphs().size());
        for (XWPFRun r : p.getRuns()) {

            System.out.println("ovoooooooooooooooooooooooooo je br"+p.getRuns().size());
            r.setText(rec,0);

        }

    }*/
}
}
