package org.example.Tabela;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.example.PomocniPaket.Matriks;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
//ova klase svrha je da obezbedi 2DArray koji ce biti ispunjen Tabla iz Word Dokumenta 
//Method koji u Poredjuje Template sa Tablom
//
public class PodaciTabelaTepmlate {
    //Uzme vrednost stringa iz celije koja se zove "Izvestaj"
    List<String> izvestaji=new ArrayList<>();
    //izvucem vrenost stringa u celiji Column govori iz kog  columna da izvlaciVrednosti
    List<String> kojiColumn=new ArrayList<>();

    //Tabela sa svim vrenostima,treba da bude koji je column i po cemu trazi
    XWPFTable table=null;

    //pokupim vrednosti iz celije,treba mi posrednik yasto ne koristim direktno XWPFT tabelou jer 
    //Moram da uradim neke funkcionalnosti za Template celiju ukupno,i "-"
    //Svrha polja kao .uporedi() vrati true onda ispunimo ovim vrednostima prema odredjenoj logici
    Matriks array2DwithString=new Matriks();


    //uporedim prvi colum templeta i baza iz word file ako su isti vracam true
    //SVRHA ako vrati true ispunimo Tabele iz Worda sa 2DArray ako je uspenso ispunjen
    public boolean uporedim(XWPFTable a) {
       /* System.out.println(a.getRow(1).getCell(0).getText());
        System.out.print(this.table.getRow(7).getCell(0).getText());*/
        ArrayList<Boolean> potvrda = new ArrayList<>();
        for (int j = 0; j < a.getRows().size(); j++) {

            if (this.table.getRows().size() - 2 == a.getRows().size()) {
                String p = a.getRow(j).getCell(0).getText();
                String h = this.table.getRow((j + 2)).getCell(0).getText();
                if (p.equals(h)) {

                    potvrda.add(true);
                } else {
                    potvrda.add(false);
                }

            } else {
                potvrda.add(false);
            }


        }

        if (potvrda.contains(false)) {

            return false;
        } else {

            return true;
        }
        //
    }


   
   /* Delovi metode zbg daljeg redizajna
      1. prvo iteriramo po kolonama preko variajble Koji Column
      2.Svakom Columnu odgovara po jedan excell(Column Excela:uporedjivanje,citanje)
      3.Svaku vrednost iz Excella Uporedimo sa vrednoscu Templet Celije 
      4.Smislim funkcionalnost kako da cita samo Templet celije. Vrednosti celije:
            4.1. promeni(String koji moze u int)
            4.2. preskoci
            4.3.drugo
            4.4.slucaj kad smo stigli do kraja excell ali i dalje je null u Matrici,stavi "-"
      6.Deo koji vraca Exception za excell mislim da je ako ga ne nadju*/
    public void napunimMatricu() {
        //table iz worda vidim column i jebeno row
        int brojRow = this.table.getRows().size() - 2;
        int brojColumn = this.table.getRow(0).getTableCells().size();

//napravim 2darray te velicine
        this.array2DwithString.ispunim(brojRow, brojColumn);

        //za svaki column loopujem, moram da preskocim prvi lup jer je to Column(mozemo posmatrati kao primary
        //jer slouzi za uporedjivanje i svaka tabla je ima i jedinstven je
        for (int i = 0; i < this.izvestaji.size(); i++) {


            String imeIzvestajaIzKogaKupimoPOdatke = this.izvestaji.get(i);
            //ako celija nije prazna onda probaj da izvuces
            //jer iz nekog glupog razloga napravili su tabele koje imaju prazne Columne izmedju
            if (this.kojiColumn.get(i) =="") {
            continue;
            }

            //string vrednost koju smo dobili iz tabele pretvorimo u integer
            int numberOfColumnFromExcell = Integer.parseInt(this.kojiColumn.get(i));

            //Privremeno Menjaj!!!!!!!!!!!! nova klasa mozda ili bar method al nmg sad
            //input Colum jer po njoj iterira,path,Templet vrednost celija sa kojom poredi(XWPFTable)
            //dal napraviti novu klasu za semu celija ili veyano ya excell pa dodati metodu uporedivanja
            //Uporedjicanja ima dva Tabele iy Worda sa Templetom, Templet sa Excelom
            //bolje mozda da napravim vezano za Templet, metod stavim string uporedi sa celijom
            
            
            
            
            try {
                File file = new File(imeIzvestajaIzKogaKupimoPOdatke);   //creating a new file instance
                FileInputStream fis = new FileInputStream(file);
                XSSFWorkbook wb = new XSSFWorkbook(fis);
                XSSFSheet sheet = wb.getSheetAt(0);
                System.out.println("Ovo je vrednost od ovoga" +sheet.getLastRowNum());
                Iterator<Row> rowIterator = sheet.iterator();
                //iteriraromo po svakom redu u ekselu
                //for(i=0;i<sheet.getLastRowNum()+1;i++) Row row=
                for(int iterator_kroz_svakirowexcella=0;iterator_kroz_svakirowexcella<sheet.getLastRowNum()+1;iterator_kroz_svakirowexcella++) {
                    Row row = sheet.getRow(iterator_kroz_svakirowexcella);
                    //uzmemo vrednost celije
                    //baci error ova kad je row null row.getCell(0).getStringCellValue();
                    if(row==null){
                        continue;
                    }


                    /*System.out.println(row.getCell(0).getStringCellValue());//test da proveri da li izvlaci sve vrednosti iy excella*/
                    String u = row.getCell(0).getStringCellValue();

                   /* System.out.println(u); //test da li iy eksela uyimamo podatke prave*/
                    for (int j = 2; j < this.table.getRows().size(); j++) {
//tabela iz templeta prva dva reda preskacemo jer su podaci ya izvestaj i column

                        if (this.table.getRow(j).getCell(i+1).getText().equals(u)) {
                            double d= row.getCell(numberOfColumnFromExcell).getNumericCellValue();
                            int in=(int)d;
                            String str = String.format("%,d", in);

                            array2DwithString.Array2D.get(j-2).set(i+1, str);
                            continue;
                        } else if (this.table.getRow(j).getCell(i+1).getText().equals("preskoci")) {
                            array2DwithString.Array2D.get(j-2).set(i+1, "preskoci");
                        continue;
                        } else if (this.table.getRow(j).getCell(i+1).getText().equals("ukupno")) {
                            array2DwithString.Array2D.get(j-2).set(i+1, "ukupno");
                            continue;
                        }else if (this.table.getRow(j).getCell(i+1).getText().equals("promeni")) {
                            array2DwithString.Array2D.get(j-2).set(i+1, "promeni");
                            continue;
                        }
                        //ako imamo vrednost po kojoj trayimo po ekselu i dodjemo do kraja ynaci da je to nula i onda unostimo "-" u tabelu
                        if(iterator_kroz_svakirowexcella==sheet.getLastRowNum() && array2DwithString.Array2D.get(j-2).get(i+1)==null){
                            array2DwithString.Array2D.get(j-2).set(i+1, "-");
                        }
                    }
// ako smo iterirari kroz row excella i nijedna nije jednaka vrednosti iz Templete stavi da je "-" ako nadjemo svejedno ce promeniti

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

      //2dArray ima to string funk
        System.out.println(array2DwithString.toString());
        stampamtable();
    }
//Odstampam sadrzaj Table
    public void stampamtable(){
        int brojRow = this.table.getRows().size() ;
        int brojColumn = this.table.getRow(0).getTableCells().size();
        for (int i = 1; i <brojColumn ; i++) {
            for (int j = 2; j < brojRow; j++) {
                System.out.println(this.table.getRow(j).getCell(i).getText());
            }
            System.out.println("novired");
        }
    }
    
    public String promeni(String input){

        String output = null;
        return output;
    }
    public static Object getCellValue(Cell cell) {
        if (cell != null) {
            switch (cell.getCellType()) {
                case Cell.CELL_TYPE_STRING:
                    return cell.getStringCellValue();

                case Cell.CELL_TYPE_BOOLEAN:
                    return cell.getBooleanCellValue();

                case Cell.CELL_TYPE_NUMERIC:
                    return cell.getNumericCellValue();
            }
        }
        return null;
    }
}

