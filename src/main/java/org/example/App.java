package org.example;

import org.apache.poi.xwpf.usermodel.*;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class App {
    public static ArrayList<XWPFTable> i=new ArrayList();
    public static void readDocxTables(String docxFilePath) throws FileNotFoundException, IOException {
        XWPFDocument doc = new XWPFDocument(new FileInputStream(docxFilePath));
        for(XWPFTable table : doc.getTables()) {
            i.add(table);
            System.out.println(i);
        }
    }
    public static void main(String[] args) throws IOException {
        readDocxTables("C:\\Users\\Vaso\\Documents\\programiranje\\SanjaUploader\\Napomene 2020 Prodyna_DRAFT.docx");

    }

}
