package org.example;


import org.apache.poi.xwpf.usermodel.IBodyElement;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFTable;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;

public class App2 {

    public static void main(String[] args) throws IOException {

        String fileName = "C:\\Users\\Vaso\\Downloads\\Napomene 2020 Prodyna_DRAFT.docx";

        XWPFDocument doc = new XWPFDocument(
                Files.newInputStream(Paths.get(fileName)));

            /*XWPFWordExtractor xwpfWordExtractor = new XWPFWordExtractor(doc);
            String docText = xwpfWordExtractor.getText();
            System.out.println(docText);*/

        Iterator<IBodyElement> docElementsIterator = doc.getBodyElementsIterator(); //svaki element

        //Iterate through the list and check for table element type
        //kroz svaki element
        while (docElementsIterator.hasNext()) {
            IBodyElement docElement = docElementsIterator.next();

            //pokupimo type elementa i uporedimo da li je tabela
            if ("TABLE".equalsIgnoreCase(docElement.getElementType().name())) {
                //Get List of table and iterate it
                List<XWPFTable> xwpfTableList = docElement.getBody().getTables();
                for (XWPFTable xwpfTable : xwpfTableList) {
                    System.out.println("Total Rows : " + xwpfTable.getNumberOfRows());
                    for (int i = 0; i < xwpfTable.getRows().size(); i++) {
                        System.out.println(xwpfTable.getRow(i).getCell(0).getText());

                    }

                }
                break;
            }

        }

    }

}